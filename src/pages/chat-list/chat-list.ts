import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Thread } from '../../shared/thread';
import { Message } from '../../shared/message';
import 'rxjs/add/operator/map';
import { EditingChatPage } from '../editing-chat/editing-chat';
import { ClosedChatsPage } from '../closed-chats/closed-chats';
/*
  Generated class for the ChatList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chat-list',
  templateUrl: 'chat-list.html'
})
export class ChatListPage {
  threads$: FirebaseListObservable<any>;
  messages$: FirebaseListObservable<any>;

  messages: Message[];
  attendantUserObjectId: string;
  assistedUserUid: any;

  modalMessages: Message[];
  modalTitle: string;

  threads: Thread[];
  activeThreads: Thread[];
  thisUserThreads: Thread[] = [];
  thisUserActiveThreads: Thread[] = [];
  threadToUpdate: Thread[]
  thisUserAssistedThreads: string[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public _afdb: AngularFireDatabase) {
    
    this.threads$ = _afdb.list('chat/threads');
    this.messages$ = _afdb.list('chat/messages');
    this.threads$.map(array => (array.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime())))
      .subscribe(threads => {
        this.threads = threads;
        this.activeThreads = this.threads.filter(activeThreads => activeThreads.closingDateTime == '');
        return this.activeThreads;
      });
    this.attendantUserObjectId = this.navParams.get('currentUserObjectId');
  };

  assistCustomer(threadObjectId) {
    // console.log('assist chat thread', threadObjectId);
    this.threads$.update(threadObjectId, {
      attendantFullName: JSON.parse(localStorage.getItem('currentUser')).firstname + ' ' + JSON.parse(localStorage.getItem('currentUser')).lastname,
      attendantUserObjectId: JSON.parse(localStorage.getItem('currentUser')).objectId,
      threadStatus: 'In corso',
      openingDateTime: new Date().toString()
    });
    this.editChat(threadObjectId);
  }

  editChat(threadObjectId) {
    // console.log('from chat list' + threadObjectId);
    // console.log('this.attendantUserObjectId', this.attendantUserObjectId);
    this.threads$.subscribe(data => this.modalTitle = 'Aperta: ' + data.openingDateTime + '- Da: ' + data.name + ' - Gestita da: ' + data.attendantFullName);
    this.messages$.map(data => data.filter(data => data.threadObjectId == threadObjectId))
      .subscribe(msg => {
        this.modalMessages = msg;
      });
    this.navCtrl.push(EditingChatPage, { threadObjectId: threadObjectId });
  };

  seeChat(threadObjectId) {
    this.threads$.subscribe(data => this.modalTitle = 'Aperta: ' + data.openingDateTime + '- Da: ' + data.name + ' - Gestita da: ' + data.attendantFullName);
    this.messages$.map(data => data.filter(data => data.threadObjectId == threadObjectId))
      .subscribe(msg => {
        this.modalMessages = msg;
      });
    this.navCtrl.push(ClosedChatsPage, { threadObjectId: threadObjectId });
  };
}
