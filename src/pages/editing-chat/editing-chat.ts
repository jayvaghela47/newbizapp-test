import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Message } from '../../shared/message';
import 'rxjs/add/operator/map';
import { Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-editing-chat',
  templateUrl: 'editing-chat.html'
})
export class EditingChatPage {
  messages$: FirebaseListObservable<any>;
  threads$: FirebaseListObservable<any>;
  threadObjectId;

  messages: Message[];
  assistantMessages: Message[];
  createForm: any;
  attendantUser;
  threadIdToClose: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: FormBuilder, public _afdb: AngularFireDatabase) {
    this.threads$ = _afdb.list('chat/threads');
    this.messages$ = _afdb.list('chat/messages');
    this.createForm = fb.group({
      'message': ['', [Validators.required]],
      'dateTime': new Date().toString()
    });
    this.threadObjectId = this.navParams.get('threadObjectId');
  }

  ionViewDidLoad() {
    this.messages$.map(data => data.filter(data => data.threadObjectId == this.threadObjectId))
      .subscribe(msg => {
        this.messages = msg;
      });
  }

  sendMessage() {
    let obj = {
      threadObjectId: this.threadObjectId,
      attendantUserObjectId: JSON.parse(localStorage.getItem('currentUser')).objectId,
      name: JSON.parse(localStorage.getItem('currentUser')).firstname + ' ' + JSON.parse(localStorage.getItem('currentUser')).lastname,
      message: this.createForm.value.message,
      dateTime: new Date().toString()
    }
    this.messages$.push(obj);
    this.createForm.patchValue({
      message: "",
    });
  }

  closeChat() {
    this.navCtrl.pop();
    this.threads$.update(this.navParams.get('threadObjectId'), {
      closingDateTime: new Date().toString(),
      threadStatus: 'Chiuso',
    });
  }
}
