import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { ChatListPage } from '../../pages/chat-list/chat-list';
import { AuthService } from '../../providers/auth-service';
import { Push } from '@ionic/cloud-angular';
declare var FCMPlugin;

/*
  Generated class for the Login page.
newproject-e1b95
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  email: string;
  password: string;
  users$: FirebaseListObservable<any>;
  pushId$: FirebaseListObservable<any>;
  currentUserData;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public _afdb: AngularFireDatabase,
    private toastCtrl: ToastController,
    private _auth: AuthService,
    public push: Push
  ) { 
    this.pushId$ = _afdb.list('user_push');
    this.users$ = this._afdb.list('/user');
  }

  ionViewDidLoad() {
    this.getPudhToken();
  }

  login(value: any) {

    let incorrectCredentials = this.toastCtrl.create({
      message: 'Utente o password non corretti',
      duration: 3000,
      position: 'bottom'
    });
     this._auth.doLogin(this.email, this.password).then(authService => {
      // console.log('ecccoooooooo: ', JSON.stringify(authService));
      this.users$.map(users => {
        if (!localStorage.getItem('token')) {
          this.getPudhToken();
        }
        return users.filter(data => data.uid == authService.uid)
      })
        .take(1).subscribe(data => {
           console.log('data: ', data);
           console.log('this: ', this);
          this.currentUserData = {
            firstname: data[0].firstname,
            lastname: data[0].lastname,
            objectId: data[0].$key,
            uid: data[0].uid
          }
          // console.log('this.currentUserData',this.currentUserData);
          if(localStorage.getItem('currentUser')){
            localStorage.removeItem('currentUser');
          }
          localStorage.setItem('currentUser', JSON.stringify(this.currentUserData));
          let obj = {
            user_id: authService.uid,
            pushId: localStorage.getItem('token'),
            dateTime: new Date().toString(),
            platform: localStorage.getItem('platform'),
            deviceId: localStorage.getItem('deviceId')
          }
          this.pushId$.push(obj).then(item => {
            // console.log('pushid itme', item.key);
            localStorage.setItem('newUserPushIdObjectId', item.key);
            this.navCtrl.setRoot(ChatListPage, {currentUserObjectId: data[0].$key});
          })
        });
    }, error => {
      alert(error);
      incorrectCredentials.present();
    });
  }

  resetPassword() {
    let passwordResetSent = this.toastCtrl.create({
      message: 'Abbiamo inviato una email al tuo indirizzo per resettare la password.',
      duration: 3000,
      position: 'bottom'
    });

    let noEmailFound = this.toastCtrl.create({
      message: 'Inserisci la tua email di iscrizione nel campo email',
      duration: 5000,
      position: 'bottom'
    }); 

    if (this.email) {
      this._auth.resetPassword(this.email);
      passwordResetSent.present();
    } else {
      noEmailFound.present();
    }
  }

  getPudhToken() {
    if(typeof(FCMPlugin) != 'undefined') { 
      FCMPlugin.getToken((t) => {
          console.log('token is:', t);
          if (localStorage.removeItem('token')) {
            localStorage.removeItem('token');
          }
          localStorage.setItem('token', t);
        },
        (e) => {
          console.log('Token error is: ', e);
        }
      );
    }
  }
}
