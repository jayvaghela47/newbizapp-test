import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Message } from '../../shared/message';
import 'rxjs/add/operator/map';

/*
  Generated class for the ClosedChats page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-closed-chats',
  templateUrl: 'closed-chats.html'
})
export class ClosedChatsPage {
  messages$: FirebaseListObservable<any>;
  threads$: FirebaseListObservable<any>;
  
  messages: Message[];
  assistantMessages: Message[];
  attendantUser;
  threadIdToClose: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public _afdb: AngularFireDatabase) {
    this.threads$ = _afdb.list('chat/threads');
    this.attendantUser = JSON.parse(localStorage.getItem('currentUser')).objectId;

    this.messages$ = _afdb.list('chat/messages');
  }

  ionViewDidLoad() {
      this.messages$.map(data => data.filter(data => data.threadObjectId == this.navParams.get('threadObjectId')))
        .subscribe(msg => {
          console.log('messages'+msg);
          this.messages = msg;
        });
  }
}
