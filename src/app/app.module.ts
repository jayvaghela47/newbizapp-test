import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AuthService } from '../providers/auth-service';
import { Badge } from '@ionic-native/badge';
import { BrowserModule } from '@angular/platform-browser';

//Cloud client
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

//pages
import { ChatListPage } from '../pages/chat-list/chat-list';
import { EditingChatPage } from '../pages/editing-chat/editing-chat';
import { ClosedChatsPage } from '../pages/closed-chats/closed-chats';
import { LoginPage } from '../pages/login/login';

//Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyAUVaQbDZzQUE4x8Kz3qNcSp6fddVS9lcs",
  authDomain: "newproject-e1b95.firebaseapp.com",
  databaseURL: "https://newproject-e1b95.firebaseio.com",
  storageBucket: "newproject-e1b95.appspot.com",
  messagingSenderId: "525749201321"
};

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'b916d55f'
  },
  'push': {
    'sender_id': '525749201321',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    ChatListPage,
    LoginPage,
    EditingChatPage,
    ClosedChatsPage,
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    CloudModule.forRoot(cloudSettings),
    BrowserModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ChatListPage,
    LoginPage,
    EditingChatPage,
    ClosedChatsPage,
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }, AuthService, Badge]
})
export class AppModule { }
