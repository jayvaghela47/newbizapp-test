import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, ToastController } from 'ionic-angular';
import { StatusBar, Splashscreen, Device, AppVersion } from 'ionic-native';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../providers/auth-service';
import { Badge } from '@ionic-native/badge';
import { Push, Deploy } from '@ionic/cloud-angular';
import { Observable } from 'rxjs/Observable';

import { ChatListPage } from '../pages/chat-list/chat-list';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = LoginPage;
  pages: Array<{ title: string, component: any }>;
  isAuthenticated: any;
  plt: string;
  threads$: FirebaseListObservable<any>;
  user_push$: FirebaseListObservable<any>;
  private _afAuth: Observable<firebase.User>
  pageStatus: boolean = true;

  constructor(
    public platform: Platform,
    public _afdb: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private _auth: AuthService,
    public badge: Badge,
    public push: Push,
    public deploy: Deploy,
    private readonly loadingCtrl: LoadingController,
    private readonly toastCtrl: ToastController
  ) {
    this.threads$ = _afdb.list('/chat/threads');
    this.user_push$ = _afdb.list('/user_push');

    this.threads$.map(threads => {
      return threads.filter(unassistedThreads => unassistedThreads.threadStatus == 'In attesa')
    }).subscribe(unassistedThreadNumber => {
      badge.set(unassistedThreadNumber.length);
    });

    this.initializeApp();

    this.pages = [
      { title: 'Login', component: LoginPage },
      { title: 'Lista delle chat', component: ChatListPage }
    ];

    this._afAuth = afAuth.authState;
    this._afAuth.subscribe(auth => {
      if (auth) {
        this.isAuthenticated = true;
        this.nav.setRoot(ChatListPage);
      }
    });

    // this.deploy.getSnapshots().then((snapshots) => {
    //   console.log('Snapshots', snapshots);
    // });
    // snapshots will be an array of snapshot uuids
    // this.deploy.info().then((x) => {
    //   localStorage.setItem('SoftwareVersion', JSON.stringify(x.deploy_uuid));
    //   console.log('Current snapshot infos', x);
    // });
    // this.checkForUpdate();
  }

  checkForUpdate() {
    const checking = this.loadingCtrl.create({
      content: 'Controllo aggiornamenti...'
    });
    checking.present();

    this.deploy.check().then((snapshotAvailable: boolean) => {
      checking.dismiss();
      if (snapshotAvailable) {
        this.downloadAndInstall();
      }
      else {
        const toast = this.toastCtrl.create({
          message: 'Nessun aggiornamento disponibile',
          duration: 3000
        });
        toast.present();
      }
    });
    this.deploy.getSnapshots().then((snapshots) => {
      console.log('Snapshots', snapshots);
      // snapshots will be an array of snapshot uuids
      this.deploy.info().then((x) => {
        console.log('Current snapshot infos', x);
        for (let suuid of snapshots) {
          if (suuid !== x.deploy_uuid) {
            this.deploy.deleteSnapshot(suuid);
          }
        }
      });
    });
  }

  private downloadAndInstall() {
    this.logout();
    const updating = this.loadingCtrl.create({
      content: "Aggiorno l'app..."
    });
    updating.present();
    this.deploy.download().then(() => this.deploy.extract()).then(() => this.deploy.load());
    AppVersion.getVersionNumber().then((s) => {
      localStorage.setItem('SoftwareVersion', s);
    });
    console.log('version', localStorage.getItem('SoftwareVersion'));
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();
      localStorage.setItem('deviceId', Device.uuid);
      if (this.platform.is('android')) {
        this.plt = 'android';
      } else {
        this.plt = 'ios';
      }
      localStorage.setItem('platform', this.plt);
    });
  }

  logout() {
    this._auth.doLogout();
    this.nav.setRoot(this.rootPage);
    this.isAuthenticated = false;
    if (localStorage.getItem('token')) {
      this._afdb.object('/user_push/' + localStorage.getItem('token')).remove();
    }
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  softwareVersion() {
    alert('Versione id: ' + localStorage.getItem('SoftwareVersion'));
  }
}