import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

  public fireAuth: any;

  constructor(public _afdb: AngularFireDatabase) {
    this.fireAuth = firebase.auth();
  }

  doLogin(email: string, password: string): any {
    return this.fireAuth.signInWithEmailAndPassword(email, password);
  }

  resetPassword(email: string): any {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  doLogout(): any {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    if(localStorage.getItem('newUserPushIdObjectId')) {
      this._afdb.object('user_push/' + localStorage.getItem('newUserPushIdObjectId')).remove();
      localStorage.removeItem('newUserPushIdObjectId');
    }
    return this.fireAuth.signOut();
  }
}