webpackJsonp([0],{

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(908);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var AuthService = (function () {
    function AuthService(_afdb) {
        this._afdb = _afdb;
        this.fireAuth = __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth();
    }
    AuthService.prototype.doLogin = function (email, password) {
        return this.fireAuth.signInWithEmailAndPassword(email, password);
    };
    AuthService.prototype.resetPassword = function (email) {
        return this.fireAuth.sendPasswordResetEmail(email);
    };
    AuthService.prototype.doLogout = function () {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        if (localStorage.getItem('newUserPushIdObjectId')) {
            this._afdb.object('user_push/' + localStorage.getItem('newUserPushIdObjectId')).remove();
            localStorage.removeItem('newUserPushIdObjectId');
        }
        return this.fireAuth.signOut();
    };
    return AuthService;
}());
AuthService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
], AuthService);

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__editing_chat_editing_chat__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__closed_chats_closed_chats__ = __webpack_require__(576);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the ChatList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var ChatListPage = (function () {
    function ChatListPage(navCtrl, navParams, _afdb) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._afdb = _afdb;
        this.thisUserThreads = [];
        this.thisUserActiveThreads = [];
        this.thisUserAssistedThreads = [];
        this.threads$ = _afdb.list('chat/threads');
        this.messages$ = _afdb.list('chat/messages');
        this.threads$.map(function (array) { return (array.sort(function (a, b) { return new Date(b.date).getTime() - new Date(a.date).getTime(); })); })
            .subscribe(function (threads) {
            _this.threads = threads;
            _this.activeThreads = _this.threads.filter(function (activeThreads) { return activeThreads.closingDateTime == ''; });
            return _this.activeThreads;
        });
        this.attendantUserObjectId = this.navParams.get('currentUserObjectId');
    }
    ;
    ChatListPage.prototype.assistCustomer = function (threadObjectId) {
        // console.log('assist chat thread', threadObjectId);
        this.threads$.update(threadObjectId, {
            attendantFullName: JSON.parse(localStorage.getItem('currentUser')).firstname + ' ' + JSON.parse(localStorage.getItem('currentUser')).lastname,
            attendantUserObjectId: JSON.parse(localStorage.getItem('currentUser')).objectId,
            threadStatus: 'In corso',
            openingDateTime: new Date().toString()
        });
        this.editChat(threadObjectId);
    };
    ChatListPage.prototype.editChat = function (threadObjectId) {
        var _this = this;
        // console.log('from chat list' + threadObjectId);
        // console.log('this.attendantUserObjectId', this.attendantUserObjectId);
        this.threads$.subscribe(function (data) { return _this.modalTitle = 'Aperta: ' + data.openingDateTime + '- Da: ' + data.name + ' - Gestita da: ' + data.attendantFullName; });
        this.messages$.map(function (data) { return data.filter(function (data) { return data.threadObjectId == threadObjectId; }); })
            .subscribe(function (msg) {
            _this.modalMessages = msg;
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__editing_chat_editing_chat__["a" /* EditingChatPage */], { threadObjectId: threadObjectId });
    };
    ;
    ChatListPage.prototype.seeChat = function (threadObjectId) {
        var _this = this;
        this.threads$.subscribe(function (data) { return _this.modalTitle = 'Aperta: ' + data.openingDateTime + '- Da: ' + data.name + ' - Gestita da: ' + data.attendantFullName; });
        this.messages$.map(function (data) { return data.filter(function (data) { return data.threadObjectId == threadObjectId; }); })
            .subscribe(function (msg) {
            _this.modalMessages = msg;
        });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__closed_chats_closed_chats__["a" /* ClosedChatsPage */], { threadObjectId: threadObjectId });
    };
    ;
    return ChatListPage;
}());
ChatListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-chat-list',template:/*ion-inline-start:"D:\Notif\src\pages\chat-list\chat-list.html"*/'<!--\n\n  Generated template for the ChatList page.\n\n\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n<ion-header>\n\n  <ion-navbar>\n\n    <button menuToggle ion-button >\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Lista delle chat</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="cards-bg social-cards">\n\n  <ion-list-header>\n\n    NUOVE CHAT\n\n  </ion-list-header>\n\n  <ion-card *ngFor="let activeThread of activeThreads" (click)="assistCustomer(activeThread.$key)">\n\n    <ion-item *ngIf="activeThread.threadStatus==\'In attesa\'">\n\n      <ion-avatar item-left>\n\n        <img src="assets/images/chat_to_be_assisted_icon.png">\n\n      </ion-avatar>\n\n      <h2>{{activeThread.name}}</h2>\n\n      <p>Data apertura: <strong>{{activeThread.date | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n      <p>Città(presunta): <strong>{{activeThread.city}}</strong></p>\n\n    </ion-item>\n\n  </ion-card>\n\n  <ion-list-header>\n\n    CHAT CHE SEGUI\n\n  </ion-list-header>\n\n  <ion-card *ngFor="let activeThread of activeThreads" (click)="editChat(activeThread.$key)">\n\n    <ion-item *ngIf="activeThread.threadStatus==\'In corso\' && activeThread.attendantUserObjectId == attendantUserObjectId">\n\n      <ion-avatar item-left>\n\n        <img src="assets/images/chat_assisted_by_you_icon.png">\n\n      </ion-avatar>\n\n      <h2>{{activeThread.name}}</h2>\n\n      <p>Data apertura: <strong>{{activeThread.date | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n      <p>Data risposta: <strong>{{activeThread.openingDateTime | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n      <p>Città(presunta): <strong>{{activeThread.city}}</strong></p>\n\n    </ion-item>\n\n  </ion-card>\n\n  <ion-list-header>\n\n    CHAT GIA\' ASSISTITE\n\n  </ion-list-header>\n\n  <ion-card *ngFor="let activeThread of activeThreads" (click)="editChat(activeThread.$key)">\n\n    <ion-item *ngIf="activeThread.threadStatus==\'In corso\' && activeThread.attendantUserObjectId != attendantUserObjectId">\n\n      <ion-avatar item-left>\n\n        <img src="assets/images/chat_assisted_by_others_icon.jpg">\n\n      </ion-avatar>\n\n      <h2>{{activeThread.name}}</h2>\n\n      <p>Data apertura: <strong>{{activeThread.date | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n      <p>Città(presunta): <strong>{{activeThread.city}}</strong></p>\n\n      <p>Seguito da: <strong>{{activeThread.attendantFullName}}</strong></p>\n\n      <p>Data risposta: <strong>{{activeThread.openingDateTime | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n    </ion-item>\n\n  </ion-card>\n\n  <ion-list-header>\n\n    CHAT CONCLUSE\n\n  </ion-list-header>\n\n  <ion-card *ngFor="let thread of threads" (click)="seeChat(thread.$key)">\n\n    <ion-item *ngIf="thread.threadStatus==\'Chiuso\'">\n\n      <ion-avatar item-left>\n\n        <img src="assets/images/chat_closed_icon.png">\n\n      </ion-avatar>\n\n      <h2>{{thread.name}}</h2>\n\n      <p>Data apertura: <strong>{{thread.date | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n      <p>Città(presunta): <strong>{{thread.city}}</strong></p>\n\n      <p>Seguito da: <strong>{{thread.attendantFullName}}</strong></p>\n\n      <p>Data risposta: <strong>{{thread.openingDateTime | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n      <p>Concluso: <strong>{{thread.closingDateTime | date: \'dd/MM/yyyy - HH:mm\'}}</strong></p>\n\n    </ion-item>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Notif\src\pages\chat-list\chat-list.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
], ChatListPage);

//# sourceMappingURL=chat-list.js.map

/***/ }),

/***/ 232:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 232;

/***/ }),

/***/ 275:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 275;

/***/ }),

/***/ 575:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditingChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EditingChatPage = (function () {
    function EditingChatPage(navCtrl, navParams, fb, _afdb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this._afdb = _afdb;
        this.threads$ = _afdb.list('chat/threads');
        this.messages$ = _afdb.list('chat/messages');
        this.createForm = fb.group({
            'message': ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            'dateTime': new Date().toString()
        });
        this.threadObjectId = this.navParams.get('threadObjectId');
    }
    EditingChatPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.messages$.map(function (data) { return data.filter(function (data) { return data.threadObjectId == _this.threadObjectId; }); })
            .subscribe(function (msg) {
            _this.messages = msg;
        });
    };
    EditingChatPage.prototype.sendMessage = function () {
        var obj = {
            threadObjectId: this.threadObjectId,
            attendantUserObjectId: JSON.parse(localStorage.getItem('currentUser')).objectId,
            name: JSON.parse(localStorage.getItem('currentUser')).firstname + ' ' + JSON.parse(localStorage.getItem('currentUser')).lastname,
            message: this.createForm.value.message,
            dateTime: new Date().toString()
        };
        this.messages$.push(obj);
        this.createForm.patchValue({
            message: "",
        });
    };
    EditingChatPage.prototype.closeChat = function () {
        this.navCtrl.pop();
        this.threads$.update(this.navParams.get('threadObjectId'), {
            closingDateTime: new Date().toString(),
            threadStatus: 'Chiuso',
        });
    };
    return EditingChatPage;
}());
EditingChatPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-editing-chat',template:/*ion-inline-start:"D:\Notif\src\pages\editing-chat\editing-chat.html"*/'<!--\n\n  Generated template for the ChatThread page.\n\n\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Chat</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button color="default" (click)="closeChat()">\n\n        Concludi chat\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <div *ngFor="let message of messages">\n\n\n\n    <ion-card ngClass="customPadding mineBlock">\n\n      <ion-item *ngIf="message.attendantUserObjectId">\n\n        <ion-avatar item-left [ngSwitch]="message.name">\n\n          <img src="assets/images/attendants/{{message.name.replace(\' \',\'\')}}.png">\n\n        </ion-avatar>\n\n        <h2>{{message.name}}</h2><strong><p>{{message.dateTime | date: \'dd/MM/yyyy - HH:mm\'}}</p></strong>\n\n        <p>{{message.message}}</p>\n\n      </ion-item>\n\n    </ion-card>\n\n\n\n    <ion-card ngClass="customPadding oppositeBlock">\n\n      <ion-item *ngIf="!message.attendantUserObjectId">\n\n        <ion-avatar item-left>\n\n          <img src="assets/images/chat_assisted_by_you_icon.png">\n\n        </ion-avatar>\n\n        <h2>{{message.name}}</h2><strong><p>{{message.dateTime | date: \'dd/MM/yyyy - HH:mm\'}}</p></strong>\n\n        <p>{{message.message}}</p>\n\n      </ion-item>\n\n    </ion-card>\n\n\n\n  </div>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar position="bottom">\n\n    <form [formGroup]="createForm" (ngSubmit)="sendMessage(createForm.value)">\n\n      <ion-item>\n\n        <ion-input type="text" placeholder="Scrivi un messaggio" formControlName="message"></ion-input>\n\n        <button ion-button item-right type="submit" [disabled]="!createForm.valid"><ion-icon name="md-send"></ion-icon></button>\n\n      </ion-item>\n\n    </form>\n\n  </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\Notif\src\pages\editing-chat\editing-chat.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
], EditingChatPage);

//# sourceMappingURL=editing-chat.js.map

/***/ }),

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClosedChatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ClosedChats page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var ClosedChatsPage = (function () {
    function ClosedChatsPage(navCtrl, navParams, _afdb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._afdb = _afdb;
        this.threads$ = _afdb.list('chat/threads');
        this.attendantUser = JSON.parse(localStorage.getItem('currentUser')).objectId;
        this.messages$ = _afdb.list('chat/messages');
    }
    ClosedChatsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.messages$.map(function (data) { return data.filter(function (data) { return data.threadObjectId == _this.navParams.get('threadObjectId'); }); })
            .subscribe(function (msg) {
            console.log('messages' + msg);
            _this.messages = msg;
        });
    };
    return ClosedChatsPage;
}());
ClosedChatsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-closed-chats',template:/*ion-inline-start:"D:\Notif\src\pages\closed-chats\closed-chats.html"*/'<!--\n\n  Generated template for the ChatThread page.\n\n\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Chat</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div *ngFor="let message of messages">\n\n    <ion-card ngClass="customPadding mineBlock">\n\n      <ion-item *ngIf="message.attendantUserObjectId">\n\n        <ion-avatar item-left>\n\n            <img src="assets/images/attendants/{{message.name.replace(\' \',\'\')}}.png">\n\n        </ion-avatar>\n\n        <h2>{{message.name}}</h2><strong><p>{{message.dateTime | date: \'dd/MM/yyyy - HH:mm\'}}</p></strong>\n\n        <p>{{message.message}}</p>\n\n      </ion-item>\n\n    </ion-card>\n\n\n\n    <ion-card ngClass="customPadding oppositeBlock">\n\n      <ion-item *ngIf="message.attendantUserObjectId == null">\n\n        <ion-avatar item-left>\n\n          <img src="assets/images/chat_assisted_by_you_icon.png">\n\n        </ion-avatar>\n\n        <h2>{{message.name}}</h2><strong><p>{{message.dateTime | date: \'dd/MM/yyyy - HH:mm\'}}</p></strong>\n\n        <p>{{message.message}}</p>\n\n      </ion-item>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\Notif\src\pages\closed-chats\closed-chats.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
], ClosedChatsPage);

//# sourceMappingURL=closed-chats.js.map

/***/ }),

/***/ 577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_chat_list_chat_list__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_cloud_angular__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the Login page.
newproject-e1b95
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var LoginPage = (function () {
    function LoginPage(platform, navCtrl, navParams, _afdb, toastCtrl, _auth, push) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._afdb = _afdb;
        this.toastCtrl = toastCtrl;
        this._auth = _auth;
        this.push = push;
        this.pushId$ = _afdb.list('user_push');
        this.users$ = this._afdb.list('/user');
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        this.getPudhToken();
    };
    LoginPage.prototype.login = function (value) {
        var _this = this;
        var incorrectCredentials = this.toastCtrl.create({
            message: 'Utente o password non corretti',
            duration: 3000,
            position: 'bottom'
        });
        this._auth.doLogin(this.email, this.password).then(function (authService) {
            // console.log('ecccoooooooo: ', JSON.stringify(authService));
            _this.users$.map(function (users) {
                if (!localStorage.getItem('token')) {
                    _this.getPudhToken();
                }
                return users.filter(function (data) { return data.uid == authService.uid; });
            })
                .take(1).subscribe(function (data) {
                console.log('data: ', data);
                console.log('this: ', _this);
                _this.currentUserData = {
                    firstname: data[0].firstname,
                    lastname: data[0].lastname,
                    objectId: data[0].$key,
                    uid: data[0].uid
                };
                // console.log('this.currentUserData',this.currentUserData);
                if (localStorage.getItem('currentUser')) {
                    localStorage.removeItem('currentUser');
                }
                localStorage.setItem('currentUser', JSON.stringify(_this.currentUserData));
                var obj = {
                    user_id: authService.uid,
                    pushId: localStorage.getItem('token'),
                    dateTime: new Date().toString(),
                    platform: localStorage.getItem('platform'),
                    deviceId: localStorage.getItem('deviceId')
                };
                _this.pushId$.push(obj).then(function (item) {
                    // console.log('pushid itme', item.key);
                    localStorage.setItem('newUserPushIdObjectId', item.key);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_chat_list_chat_list__["a" /* ChatListPage */], { currentUserObjectId: data[0].$key });
                });
            });
        }, function (error) {
            alert(error);
            incorrectCredentials.present();
        });
    };
    LoginPage.prototype.resetPassword = function () {
        var passwordResetSent = this.toastCtrl.create({
            message: 'Abbiamo inviato una email al tuo indirizzo per resettare la password.',
            duration: 3000,
            position: 'bottom'
        });
        var noEmailFound = this.toastCtrl.create({
            message: 'Inserisci la tua email di iscrizione nel campo email',
            duration: 5000,
            position: 'bottom'
        });
        if (this.email) {
            this._auth.resetPassword(this.email);
            passwordResetSent.present();
        }
        else {
            noEmailFound.present();
        }
    };
    LoginPage.prototype.getPudhToken = function () {
        if (typeof (FCMPlugin) != 'undefined') {
            FCMPlugin.getToken(function (t) {
                console.log('token is:', t);
                if (localStorage.removeItem('token')) {
                    localStorage.removeItem('token');
                }
                localStorage.setItem('token', t);
            }, function (e) {
                console.log('Token error is: ', e);
            });
        }
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"D:\Notif\src\pages\login\login.html"*/'<!--\n\n  Generated template for the Login page.\n\n\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Login</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div style="margin-top: 30%" center>\n\n    <img style="margin-left: 15%; width: 70%" src="assets/images/Logo.png" />\n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-label>Email</ion-label>\n\n        <ion-input type="text" value="" [(ngModel)]="email"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Password</ion-label>\n\n        <ion-input type="password" value="" [(ngModel)]="password"></ion-input>\n\n      </ion-item>\n\n    </ion-list>\n\n    <div>\n\n      <button ion-button block color="primary" (click)="login()">Entra</button>\n\n    </div>\n\n    <a href="#" center (click)="resetPassword()">Ho dimenticato la password</a>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\Notif\src\pages\login\login.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_cloud_angular__["c" /* Push */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 578:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(583);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 583:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__(620);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_badge__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_cloud_angular__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_chat_list_chat_list__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_editing_chat_editing_chat__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_closed_chats_closed_chats__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angularfire2_database__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angularfire2_auth__ = __webpack_require__(530);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






//Cloud client

//pages




//Firebase



var firebaseConfig = {
    apiKey: "AIzaSyAUVaQbDZzQUE4x8Kz3qNcSp6fddVS9lcs",
    authDomain: "newproject-e1b95.firebaseapp.com",
    databaseURL: "https://newproject-e1b95.firebaseio.com",
    storageBucket: "newproject-e1b95.appspot.com",
    messagingSenderId: "525749201321"
};
var cloudSettings = {
    'core': {
        'app_id': 'b916d55f'
    },
    'push': {
        'sender_id': '525749201321',
        'pluginConfig': {
            'ios': {
                'badge': true,
                'sound': true
            },
            'android': {
                'iconColor': '#343434'
            }
        }
    }
};
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_7__pages_chat_list_chat_list__["a" /* ChatListPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_editing_chat_editing_chat__["a" /* EditingChatPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_closed_chats_closed_chats__["a" /* ClosedChatsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */]),
            __WEBPACK_IMPORTED_MODULE_11_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
            __WEBPACK_IMPORTED_MODULE_12_angularfire2_database__["b" /* AngularFireDatabaseModule */],
            __WEBPACK_IMPORTED_MODULE_13_angularfire2_auth__["b" /* AngularFireAuthModule */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_cloud_angular__["a" /* CloudModule */].forRoot(cloudSettings),
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["a" /* BrowserModule */],
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_7__pages_chat_list_chat_list__["a" /* ChatListPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_editing_chat_editing_chat__["a" /* EditingChatPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_closed_chats_closed_chats__["a" /* ClosedChatsPage */],
        ],
        providers: [{ provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* IonicErrorHandler */] }, __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_badge__["a" /* Badge */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 620:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_native__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_badge__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_cloud_angular__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_chat_list_chat_list__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(577);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = (function () {
    function MyApp(platform, _afdb, afAuth, _auth, badge, push, deploy, loadingCtrl, toastCtrl) {
        var _this = this;
        this.platform = platform;
        this._afdb = _afdb;
        this.afAuth = afAuth;
        this._auth = _auth;
        this.badge = badge;
        this.push = push;
        this.deploy = deploy;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */];
        this.pageStatus = true;
        this.threads$ = _afdb.list('/chat/threads');
        this.user_push$ = _afdb.list('/user_push');
        this.threads$.map(function (threads) {
            return threads.filter(function (unassistedThreads) { return unassistedThreads.threadStatus == 'In attesa'; });
        }).subscribe(function (unassistedThreadNumber) {
            badge.set(unassistedThreadNumber.length);
        });
        this.initializeApp();
        this.pages = [
            { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */] },
            { title: 'Lista delle chat', component: __WEBPACK_IMPORTED_MODULE_8__pages_chat_list_chat_list__["a" /* ChatListPage */] }
        ];
        this._afAuth = afAuth.authState;
        this._afAuth.subscribe(function (auth) {
            if (auth) {
                _this.isAuthenticated = true;
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_chat_list_chat_list__["a" /* ChatListPage */]);
            }
        });
        // this.deploy.getSnapshots().then((snapshots) => {
        //   console.log('Snapshots', snapshots);
        // });
        // snapshots will be an array of snapshot uuids
        // this.deploy.info().then((x) => {
        //   localStorage.setItem('SoftwareVersion', JSON.stringify(x.deploy_uuid));
        //   console.log('Current snapshot infos', x);
        // });
        // this.checkForUpdate();
    }
    MyApp.prototype.checkForUpdate = function () {
        var _this = this;
        var checking = this.loadingCtrl.create({
            content: 'Controllo aggiornamenti...'
        });
        checking.present();
        this.deploy.check().then(function (snapshotAvailable) {
            checking.dismiss();
            if (snapshotAvailable) {
                _this.downloadAndInstall();
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Nessun aggiornamento disponibile',
                    duration: 3000
                });
                toast.present();
            }
        });
        this.deploy.getSnapshots().then(function (snapshots) {
            console.log('Snapshots', snapshots);
            // snapshots will be an array of snapshot uuids
            _this.deploy.info().then(function (x) {
                console.log('Current snapshot infos', x);
                for (var _i = 0, snapshots_1 = snapshots; _i < snapshots_1.length; _i++) {
                    var suuid = snapshots_1[_i];
                    if (suuid !== x.deploy_uuid) {
                        _this.deploy.deleteSnapshot(suuid);
                    }
                }
            });
        });
    };
    MyApp.prototype.downloadAndInstall = function () {
        var _this = this;
        this.logout();
        var updating = this.loadingCtrl.create({
            content: "Aggiorno l'app..."
        });
        updating.present();
        this.deploy.download().then(function () { return _this.deploy.extract(); }).then(function () { return _this.deploy.load(); });
        __WEBPACK_IMPORTED_MODULE_2_ionic_native__["a" /* AppVersion */].getVersionNumber().then(function (s) {
            localStorage.setItem('SoftwareVersion', s);
        });
        console.log('version', localStorage.getItem('SoftwareVersion'));
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            __WEBPACK_IMPORTED_MODULE_2_ionic_native__["f" /* StatusBar */].styleDefault();
            __WEBPACK_IMPORTED_MODULE_2_ionic_native__["e" /* Splashscreen */].hide();
            localStorage.setItem('deviceId', __WEBPACK_IMPORTED_MODULE_2_ionic_native__["b" /* Device */].uuid);
            if (_this.platform.is('android')) {
                _this.plt = 'android';
            }
            else {
                _this.plt = 'ios';
            }
            localStorage.setItem('platform', _this.plt);
        });
    };
    MyApp.prototype.logout = function () {
        this._auth.doLogout();
        this.nav.setRoot(this.rootPage);
        this.isAuthenticated = false;
        if (localStorage.getItem('token')) {
            this._afdb.object('/user_push/' + localStorage.getItem('token')).remove();
        }
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.softwareVersion = function () {
        alert('Versione id: ' + localStorage.getItem('SoftwareVersion'));
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"D:\Notif\src\app\app.html"*/'<div>\n    <ion-menu [content]="content">\n        <ion-header>\n        <ion-toolbar>\n            <ion-title>Menu</ion-title>\n        </ion-toolbar>\n        </ion-header>\n\n        <ion-content>\n        <ion-list>\n            <button *ngIf="isAuthenticated == false" menuClose ion-item (click)="logout()">\n            Entra\n            </button>\n            <button *ngIf="isAuthenticated == true" menuClose ion-item (click)="logout()">\n            Esci\n            </button>\n            <button menuClose ion-item (click)="openPage(pages[1])">\n            Lista delle chat\n            </button>\n            <button menuClose ion-item (click)="softwareVersion()">\n            Versione Software\n            </button>\n            <button menuClose ion-item (click)="checkForUpdate()">\n            Aggiornamento Software\n            </button>\n        </ion-list>\n        </ion-content>\n    </ion-menu>\n    </div>\n    <!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\Notif\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_badge__["a" /* Badge */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_cloud_angular__["c" /* Push */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_cloud_angular__["b" /* Deploy */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ })

},[578]);
//# sourceMappingURL=main.js.map